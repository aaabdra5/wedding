(function() {
    var params = getUrlVars();
    var id = params['id'];

    var title = document.getElementById('title');

    if (!id) {
        // что-то делаем
        console.error('id is empty');
        return;
    }

    id = id.toLowerCase();

    var filteredData = getData().filter(function(el) {
        return el.id == id;
    });

    if (!filteredData || !filteredData.length) {
        // что-то делаем
        console.error('data is empty');
        return;
    }

    var item = filteredData[0];

    console.log(item.name);
    title.innerHTML = item.name;
})()

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&|#]*)/gi,
    function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function getData() {
    return [
        {
            id: "parents-alsu",
            name: 'Дорогие Папа, Мама и Эмиль'
        },
        {
            id: "hanbikovy",
            name: 'Дорогие Марсель, Фируза, Денис и Диана!'
        },
        {
            id: "asya",
            name: 'Дорогие дядя Равиль и Ася!'
        },
        {
            id: "ansar",
            name: 'Дорогой дядя Ансар!'
        },
        {
            id: "renata",
            name: 'Дорогие Кирилл и Рената!'
        },
        {
            id: "tatarka",
            name: 'Дорогие Денис и Кристина!'
        },
        {
            id: "dimasya",
            name: 'Дорогой Дима!'
        },
        {
            id: "yagudinka",
            name: 'Дорогие Дима и Алена!'
        },
        {
            id: "rodinka",
            name: 'Дорогие Саша и Катя!'
        },
        {
            id: "lunkin",
            name: 'Дорогой Максим!'
        },
        {
            id: "kris",
            name: 'Дорогая Кристина!'
        },
        {
            id: "kalebin",
            name: 'Дорогие Андрей и Ярослава!'
        },
        {
            id: "sokol",
            name: 'Дорогие Саша и Аня!'
        },


        {
            id: "parents-nariman",
            name: 'Дорогие Папа и Мама!'
        },
        {
            id: "fifi",
            name: 'Дорогие Дамир, Лидия, Сафия, Самира и Амир!'
        },
        {
            id: "ki-zi",
            name: 'Дорогие Кирилл и Ильзира!'
        },
        {
            id: "apa",
            name: 'Дорогие апа Зоя и апа Неля!'
        },
        {
            id: "piter",
            name: 'Дорогая апа Фая!'
        },
        {
            id: "m",
            name: 'Дорогая тетя Маша!'
        },
        {
            id: 'radik',
            name: 'Дорогие Радик и Гульнара!'
        },
        {
            id: 'rafo',
            name: 'Дорогие Рафаэль и Альфия!'
        },
        {
            id: 'ramil',
            name: 'Дорогие Рамиль и Лилия!'
        },
        {
            id: 'rinat',
            name: 'Дорогой Ринат!'
        },
        {
            id: 'tuktarov-ildar',
            name: 'Дорогие Ильдар, Альбина, Рушания и Таир!'
        },
        {
            id: 'tuktarov-lily',
            name: 'Дорогие Лилия и Тимур!'
        },
        {
            id: 'tuktarov-almaz',
            name: 'Дорогие Алмаз и Джамиля!' // ?, Тимур и Руслан
        },
        {
            id: 'tuktarov-damir',
            name: 'Дорогие Дамир, Елена и Диляра!' // ?Даниил
        },
        {
            id: 'tuktarov-ruslan',
            name: 'Дорогой Руслан!'
        },
        {
            id: 'damirchik',
            name: 'Дорогие Дамир и Нурия!' //
        },
        {
            id: 'salikhov',
            name: 'Дорогие Марат, Кира, Аделя, Олег и Лилия!'
        },

        {
            id: "qqq",
            name: 'Дорогой Амаяк!'
        },
        {
            id: "vadyusha",
            name: 'Дорогие Вадим и Алиса!'
        },
        {
            id: "kazanova",
            name: 'Дорогой Артем!' //мыз
        },
        {
            id: "samson",
            name: 'Дорогие Алексей и Кристина!'
        },
        {
            id: "anton",
            name: 'Дорогой Антон!'
        },
        {
            id: "temik",
            name: 'Дорогой Артем!' //темик
        },
        {
            id: "ilya",
            name: 'Дорогой Илья!'
        },
        {
            id: "barnashev",
            name: 'Дорогие Тимур и Вика!'
        },
        {
            id: "rasulov",
            name: 'Дорогие Джавид и Натия!'
        },
        {
            id: "yura",
            name: 'Дорогой Юра!'
        },
        {
            id: "plast",
            name: 'Дорогие Евгений и Кристина!'
        }
    ];
}